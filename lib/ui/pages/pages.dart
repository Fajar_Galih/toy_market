import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:toy_market/shared/shared.dart';
import 'package:supercharged/supercharged.dart';

part 'general_page.dart';
part 'sign_in_page.dart';
